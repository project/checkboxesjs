'use strict';

/**
 * @file
 * Applies checkboxesjs to eligible checkboxes.
 */

(function ($, Drupal) {
  Drupal.behaviors.checkboxesjs = {
    attach: function (context, settings) {
      $('[data-checkboxesjs-buttons]').each(function () {
          let el = $(this);
          let buttons = el.data("checkboxesjs-buttons");
          el.prepend(
            buttons
          );
          // Prevent endless additions of buttons when autosave runs.
          el.removeAttr('data-checkboxesjs-buttons');
      });

      // $('#edit-field-findit-ages').checkboxes('range', true);

      $('[data-toggle^=checkboxes]').each(function () {
        let el = $(this);
        let actions = el.data();
        delete actions.toggle;
        for (let action in actions) {
          el.checkboxes(action, actions[action]);
        }
      });


    }
  };
})(jQuery, Drupal);
