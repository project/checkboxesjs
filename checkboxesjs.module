<?php

/**
 * @file
 * Hook implementations for checkboxesjs.
 */


use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter for options_buttons.
 *
 * Note: Not boolean_checkbox
 */
function checkboxesjs_field_widget_options_buttons_form_alter(&$element, FormStateInterface $form_state, $context) {
  // Apply only to checkbox elements and ensure that checkboxesjs is enabled on the field
  if ($element['#type'] === 'checkboxes') {
    $settings = $context['widget']->getThirdPartySettings('checkboxesjs');
    // Check to see if widget has been configured at all, and if so if disabled.
    if (isset($settings['checkboxesjs']) && (
        $settings['checkboxesjs']['enabled'] === 0 || count($element['#options']) < $settings['checkboxesjs']['minimum']
      )) {
      return;
    }
    // Include our library defined in checkboxesjs.libraries.yml.
    $element['#attached']['library'][] = 'checkboxesjs/checkboxesjs';
  }
  else {
    return;
  }

  // Include our library defined in checkboxesjs.libraries.yml.
  $element['#attached']['library'][] = 'checkboxesjs/checkboxesjs';
}

/**
 * Prepares variables for checkboxes templates.
 *
 * Default template: checkboxes.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #children, #attributes.
 *
  $element = $variables['element'];
  $variables['attributes'] = [];
  if (isset($element['#id'])) {
    $variables['attributes']['id'] = $element['#id'];
  }
  if (isset($element['#attributes']['title'])) {
    $variables['attributes']['title'] = $element['#attributes']['title'];
  }
  $variables['children'] = $element['#children'];
 */
function checkboxesjs_preprocess_checkboxes(&$variables) {

  // This makes it possible to press one checkbox, move to another and press it
  // while holding down shift and all the ones in the middle are checked also.
  $variables['attributes']['data-toggle'] = "checkboxes";
  $variables['attributes']['data-range'] = "true";

  // Range selection is good everywhere but only show "Check all" and "Toggle"
  // buttons for more than five checkboxes or if it has been enabled by field.
  $libraries = !empty($variables["element"]["#attached"]["library"]) ? $variables["element"]["#attached"]["library"] : [];
  if (!in_array("checkboxesjs/checkboxesjs", $libraries)) {
    return;
  }

  // Our JavaScript will print these buttons out, somehow in time for
  // checkboxesjs to operate on them.  The data-toggle checkboxes piece
  // documented in checkboxesjs is apparently redundant with (and causes
  // duplicate actions, which neutralizes toggle, when present) the data-toggle
  // set on the checkboxes-containing element itself, above.
  $id = $variables['attributes']['id'];
  $variables['attributes']['data-checkboxesjs-buttons'] = '<div class="checkboxesjs-buttons">
    <a class="button is-small" href="#' . $id . '" data-action="check">' . t('Check all') . '</a>
    <a class="button is-small" href="#' . $id . '" data-action="toggle">' . t('Toggle') . '</a>
  </div>';
}

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function checkboxesjs_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  if ($plugin instanceof OptionsButtonsWidget) {
    $plugin->defaultSettings(
      'checkboxesjs', 'checkboxesjs', [
        'enabled' => TRUE,
      ]
    );
    $settings = $plugin->getThirdPartySettings('checkboxesjs');
    $element['checkboxesjs']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t("Provide 'Check All' Option?"),
      '#description' => t('Should the user be presented with "Check All" and "Toggle All" Option? This option allows the user to check all options in a list of checkboxes.'),
      '#default_value' => isset($settings['checkboxesjs']['enabled']) ? $settings['checkboxesjs']['enabled'] : TRUE,
    ];
    $element['checkboxesjs']['minimum'] = [
      '#type' => 'number',
      '#title' => t("Minimum checkboxes."),
      '#description' => t('The "Check All" and "Toggle All" Option will be presented only if there\'s at least the defined number of checkboxes.'),
      '#min' => 2,
      '#default_value' => isset($settings['checkboxesjs']['minimum']) ? $settings['checkboxesjs']['minimum'] : 5,
    ];
    return $element;
  }
}
