# Installation

Until [handling of non-PHP third-party libraries is figured out by Drupal](https://agaric.gitlab.io/raw-notes/notes/2019-09-15-adding-a-javascript-library-to-a-drupal-8-module-so-it-just-works-with-composer-managed-projects/ ), which might never happen, installing this module requires jumping through some hoops:

## Step 1: Pre-requisites

Recent versions of the [Composer template for Drupal projects](https://github.com/drupal-composer/drupal-project) have this, but ensure that your project's composer.json file has `composer/installers` somewhere in the require section, something like this:

```json
    "require": {
        "composer/installers": "^1.7",
```

And an installer-path (defined under extra) is set to `"web/libraries/{$name}": ["type:drupal-library"]`, that is, there's part of your composer.json file that looks something like this:

```json
    "extra": {
        "installer-paths": {
            "web/core": ["drupal/core"],
            "web/libraries/{$name}": ["type:drupal-library"],
```

## Step 2: Define a package for the library

Now, add this to your `repositories` section, creating that section if necessary:


```json
    "repositories": {
        "rmariuzzo/checkboxes.js": {
            "type": "package",
            "package": {
                "name": "rmariuzzo/checkboxes.js",
                "version": "1.2.2",
                "type": "drupal-library",
                "dist": {
                    "url": "https://github.com/rmariuzzo/checkboxes.js/archive/v1.2.2.zip",
                    "type": "zip"
                },
                "require": {
                    "composer/installers": "^1.2.0"
                }
            }
        }
    },
```

JSON doesn't allow the final item in a set to have a trailing comma, so watch out for that, as well as keeping the curly braces matching.  I'm sorry, no human being should have to edit JSON files.
